#ifndef CALCULATORWINDOW_H
#define CALCULATORWINDOW_H

#include <QMainWindow>

namespace Ui {
class CalculatorWindow;
}

class CalculatorWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit CalculatorWindow(QWidget *parent = 0);
    ~CalculatorWindow();

private slots:
    void onDigitRelease();
    void onClearRelease();
    void onOperationRelease();
    void onEqualsRelease();
    void onChangeSignRelease();
    void onPercentRelease();
    void onPointRelease();
    void onSquareRootRelease();
    void onMemAddRelease();
    void onMemClearRelease();
    void onMemRestoreRelease();

private:
    Ui::CalculatorWindow *ui;
    double operate(double value1, char sign, double value2);
    void updateDisplay(double value);
};

#endif // CALCULATORWINDOW_H
