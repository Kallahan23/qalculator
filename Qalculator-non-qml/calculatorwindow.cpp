#include "calculatorwindow.h"
#include "ui_calculatorwindow.h"
#include <math.h>
#include <QDebug>

#define MAX_DISPLAY_LENGTH 8

enum Operation {
    add = '+',
    subtract = '-',
    multiply = 'x',
    divide = '/'
};

double calcValue = 0;
double memory = 0;
Operation selectedOperation;
bool operationTriggered = false;
bool equalsTriggered = false;

CalculatorWindow::CalculatorWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::CalculatorWindow) {
    ui->setupUi(this);

    this->setFixedSize(QSize(256, 448));

    // Set initial display value
    ui->display->setText(QString::number(calcValue));

    // Connect digit button signals/slots
    for(int i = 0; i < 10; ++i) {
        QPushButton* button = CalculatorWindow::findChild<QPushButton*>("button" + QString::number(i));
        connect(button, SIGNAL(released()), this, SLOT(onDigitRelease()));
    }

    // Connect operation button signals/slots
    connect(ui->buttonAdd, SIGNAL(released()), this, SLOT(onOperationRelease()));
    connect(ui->buttonSubtract, SIGNAL(released()), this, SLOT(onOperationRelease()));
    connect(ui->buttonMultiply, SIGNAL(released()), this, SLOT(onOperationRelease()));
    connect(ui->buttonDivide, SIGNAL(released()), this, SLOT(onOperationRelease()));
    connect(ui->buttonEquals, SIGNAL(released()), this, SLOT(onEqualsRelease()));

    connect(ui->buttonSign, SIGNAL(released()), this, SLOT(onChangeSignRelease()));
    connect(ui->buttonPercent, SIGNAL(released()), this, SLOT(onPercentRelease()));
    connect(ui->buttonPoint, SIGNAL(released()), this, SLOT(onPointRelease()));
    connect(ui->buttonSquareRoot, SIGNAL(released()), this, SLOT(onSquareRootRelease()));

    // Connect action button signals/slots
    connect(ui->buttonClear, SIGNAL(released()), this, SLOT(onClearRelease()));
    connect(ui->buttonMemAdd, SIGNAL(released()), this, SLOT(onMemAddRelease()));
    connect(ui->buttonMemClear, SIGNAL(released()), this, SLOT(onMemClearRelease()));
    connect(ui->buttonMemRestore, SIGNAL(released()), this, SLOT(onMemRestoreRelease()));
}

CalculatorWindow::~CalculatorWindow() {
    delete ui;
}

double CalculatorWindow::operate(double value1, char sign, double value2) {
    if (sign == add) {
        return value1 + value2;
    } else if (sign == subtract) {
        return value1 - value2;
    } else if (sign == multiply) {
        return value1 * value2;
    } else if (sign == divide) {
        return value1 / value2;
    }

    return value2;
}

void CalculatorWindow::updateDisplay(double value) {
    ui->display->setText(QString::number(value, 'g', MAX_DISPLAY_LENGTH));
}

void CalculatorWindow::onDigitRelease() {
    QPushButton* button = (QPushButton*) sender();
    QString buttonValue = button->text();
    QString currentValue = ui->display->text();

    if (currentValue.length() < MAX_DISPLAY_LENGTH) {
        if (currentValue == "0" || operationTriggered || equalsTriggered) {
            ui->display->setText(buttonValue);
            operationTriggered = false;
            equalsTriggered = false;
        } else {
            QString newValue = currentValue + buttonValue;
            double newValueFlt = newValue.toFloat();
            updateDisplay(newValueFlt);
        }
    }
}

void CalculatorWindow::onClearRelease() {
    ui->display->setText("0");
}

void CalculatorWindow::onOperationRelease() {
    QPushButton* button = (QPushButton*) sender();
    QString buttonValue = button->text();

    // Save current value
    if (!operationTriggered) {
        calcValue = operate(calcValue, selectedOperation, ui->display->text().toDouble());
        qDebug() << calcValue;
    }

    // Store operation
    selectedOperation = (Operation) buttonValue.toStdString()[0];

    // Display selected operation
    ui->displayOperation->setText(buttonValue);
    operationTriggered = true;
}

void CalculatorWindow::onEqualsRelease() {
    calcValue = operate(calcValue, selectedOperation, ui->display->text().toDouble());
    qDebug() << calcValue;

    // Clear operations
    selectedOperation = (Operation) 0;
    ui->displayOperation->clear();

    // Display result
    updateDisplay(calcValue);
    equalsTriggered = true;
}

void CalculatorWindow::onChangeSignRelease() {
    if (!operationTriggered) {
        updateDisplay(ui->display->text().toDouble() * -1.0);
    }
}

void CalculatorWindow::onPercentRelease() {
    if (!operationTriggered) {
        updateDisplay(ui->display->text().toDouble() * 0.01);
    }
}

void CalculatorWindow::onPointRelease() {
    QString currentValue = ui->display->text();
    if (currentValue.toStdString().find('.') == std::string::npos) {
        ui->display->setText(currentValue + ".");
    }
}

void CalculatorWindow::onSquareRootRelease() {
    updateDisplay(sqrt(calcValue));
}

void CalculatorWindow::onMemAddRelease() {
    memory = calcValue;
    ui->memoryStatus->setText("M");
}

void CalculatorWindow::onMemClearRelease() {
    memory = 0.0;
    ui->memoryStatus->clear();
}

void CalculatorWindow::onMemRestoreRelease() {
    updateDisplay(memory);
}
