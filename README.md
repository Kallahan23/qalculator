# Qalculator

Basic calculator app using Qt5: C++ and QML, built using SCons.

The app can be found in the `Qalculator` folder.

The `Qalculator-non-qml` folder contains the old version of the app: a Qt Widgets Application
