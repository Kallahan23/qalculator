import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Button {
    id: button
    Layout.fillHeight: true
    Layout.fillWidth: true

    background: Rectangle {
        color: "#d4d4d4"
        gradient: Gradient {
            GradientStop { position: 0; color: button.pressed ? "#bebebe" : "#d4d4d4" }
            GradientStop { position: 1; color: "#d4d4d4" }
        }
    }

    contentItem: Text {
        color: "black"
        text: parent.text
        font: parent.font
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
}
