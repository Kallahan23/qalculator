#include "calculator.h"
#include <math.h>
#include <QDebug>

#define MAX_DISPLAY_LENGTH 6

enum Operation {
    add = '+',
    subtract = '-',
    multiply = 'x',
    divide = '/'
};

double calcValue = 0;
double memory = 0;
Operation selectedOperation;
bool operationTriggered = false;
bool equalsTriggered = false;
bool pointTriggered = false;

Calculator::Calculator(QObject *parent) : QObject(parent) {
    setDisplayValue("0");
}

QString Calculator::displayValue() const {
   return m_displayValue;
}

void Calculator::setDisplayValue(QString newValue) {
   double newValueFlt = newValue.toDouble();
   if (m_displayValue != newValue && newValueFlt < pow(10, MAX_DISPLAY_LENGTH)) {
       qDebug() << newValue << newValueFlt;
       m_displayValue = QString::number(newValueFlt);
       displayChanged();
   }
}

QString Calculator::displaySign() const {
   return m_displaySign;
}

void Calculator::setDisplaySign(QString newValue) {
    m_displaySign = newValue;
    displaySignChanged();
}

void Calculator::enterDigit(const QString &digit) {
    if (displayValue().length() < MAX_DISPLAY_LENGTH) {
        if (digit == "." && displayValue().toStdString().find('.') == std::string::npos) {
            pointTriggered = true;
        } else if (displayValue() == "0" || operationTriggered || equalsTriggered) {
            setDisplayValue(digit);
            operationTriggered = false;
            equalsTriggered = false;
        } else if (pointTriggered) {
            setDisplayValue(displayValue() + "." + digit);
            pointTriggered = false;
        } else {
            setDisplayValue(displayValue() + digit);
        }
    }
}

void Calculator::clearDisplay() {
   setDisplayValue("0");
}

void Calculator::setOperation(const QString &sign) {
    // Save current value
    if (!operationTriggered) {
        calcValue = operate(calcValue, selectedOperation, displayValue().toDouble());
        qDebug() << calcValue << sign;
        setDisplayValue(QString::number(calcValue));
    }

    // Store operation
    selectedOperation = (Operation) sign.toStdString()[0];

    // Display selected operation
    setDisplaySign(sign);
    operationTriggered = true;
}

double Calculator::operate(double value1, char sign, double value2) {
    if (sign == add) {
        return value1 + value2;
    } else if (sign == subtract) {
        return value1 - value2;
    } else if (sign == multiply) {
        return value1 * value2;
    } else if (sign == divide) {
        return value1 / value2;
    }

    return value2;
}

void Calculator::equals() {
    calcValue = operate(calcValue, selectedOperation, displayValue().toDouble());
    qDebug() << calcValue;

    // Clear operations
    selectedOperation = (Operation) 0;
    setDisplaySign("");

    // Display result
    setDisplayValue(QString::number(calcValue));
    equalsTriggered = true;
}

void Calculator::changeSign() {
    if (!operationTriggered) {
        setDisplayValue(QString::number(displayValue().toDouble() * -1.0));
    }
}

void Calculator::percent() {
    if (!operationTriggered) {
        setDisplayValue(QString::number(displayValue().toDouble() * 0.01));
    }
}

void Calculator::squareRoot() {
    setDisplayValue(QString::number(sqrt(displayValue().toDouble())));
}

void Calculator::memAdd() {
    memory = calcValue;
}

void Calculator::memClear() {
    memory = 0.0;
}

void Calculator::memRestore() {
    setDisplayValue(QString::number(memory));
}
