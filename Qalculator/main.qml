import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Window {
    id: calculatorWindow
    visible: true
    width: 256
    height: 448
    title: qsTr("Qalculator")

    GridLayout {
        id: gridLayout
        rows: 7
        columns: 4
        anchors.fill: parent
        rowSpacing: 1
        columnSpacing: 1

        Label {
            id: displaySign
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 14
            Layout.row: 1
            Layout.column: 1
            Layout.columnSpan: 1
            Layout.fillHeight: true
            Layout.fillWidth: true
            text: calculator.displaySign
        }

        Label {
            id: display
            font.capitalization: Font.Capitalize
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            transformOrigin: Item.Center
            font.pointSize: 14
            Layout.row: 1
            Layout.column: 2
            Layout.columnSpan: 3
            Layout.fillHeight: true
            Layout.fillWidth: true
            text: calculator.displayValue
        }

        WhiteButton {
            id: buttonPoint
            text: qsTr(".")
            Layout.row: 7
            Layout.column: 3
            Layout.columnSpan: 1
        }

        WhiteButton {
            id: button0
            text: qsTr("0")
            Layout.row: 7
            Layout.column: 1
            Layout.columnSpan: 2
        }

        WhiteButton {
            id: button1
            text: qsTr("1")
            Layout.row: 6
            Layout.column: 1
            Layout.columnSpan: 1
        }

        WhiteButton {
            id: button2
            text: qsTr("2")
            Layout.row: 6
            Layout.column: 2
            Layout.columnSpan: 1
        }

        WhiteButton {
            id: button3
            text: qsTr("3")
            Layout.row: 6
            Layout.column: 3
            Layout.columnSpan: 1
        }

        WhiteButton {
            id: button4
            text: qsTr("4")
            Layout.row: 5
            Layout.column: 1
            Layout.columnSpan: 1
        }

        WhiteButton {
            id: button5
            text: qsTr("5")
            Layout.row: 5
            Layout.column: 2
            Layout.columnSpan: 1
        }

        WhiteButton {
            id: button6
            text: qsTr("6")
            Layout.row: 5
            Layout.column: 3
            Layout.columnSpan: 1
        }

        WhiteButton {
            id: button7
            text: qsTr("7")
            Layout.row: 4
            Layout.column: 1
            Layout.columnSpan: 1
        }

        WhiteButton {
            id: button8
            text: qsTr("8")
            Layout.row: 4
            Layout.column: 2
            Layout.columnSpan: 1
        }

        WhiteButton {
            id: button9
            text: qsTr("9")
            Layout.row: 4
            Layout.column: 3
            Layout.columnSpan: 1
        }

        OrangeButton {
            id: buttonEquals
            text: qsTr("=")
            Layout.row: 7
            Layout.column: 4
            Layout.columnSpan: 1
            onClicked: calculator.equals()
        }

        OrangeButton {
            id: buttonAdd
            text: qsTr("+")
            Layout.row: 6
            Layout.column: 4
            Layout.columnSpan: 1
            onClicked: calculator.setOperation(text)
        }

        OrangeButton {
            id: buttonSubtract
            text: qsTr("-")
            Layout.row: 5
            Layout.column: 4
            Layout.columnSpan: 1
            onClicked: calculator.setOperation(text)
        }

        OrangeButton {
            id: buttonMultipliy
            text: qsTr("x")
            Layout.row: 4
            Layout.column: 4
            Layout.columnSpan: 1
            onClicked: calculator.setOperation(text)
        }

        OrangeButton {
            id: buttonDivide
            text: qsTr("÷")
            Layout.row: 3
            Layout.column: 4
            Layout.columnSpan: 1
            onClicked: calculator.setOperation("/")
        }

        OrangeButton {
            id: buttonSquareRoot
            text: qsTr("√")
            Layout.row: 2
            Layout.column: 4
            Layout.columnSpan: 1
            onClicked: calculator.squareRoot()
        }

        GreyButton {
            id: buttonClear
            text: qsTr("C")
            Layout.row: 3
            Layout.column: 1
            Layout.columnSpan: 1
            onClicked: calculator.clearDisplay()
        }

        GreyButton {
            id: buttonChangeSign
            text: qsTr("+/-")
            Layout.row: 3
            Layout.column: 2
            Layout.columnSpan: 1
            onClicked: calculator.changeSign()
        }

        GreyButton {
            id: buttonPercent
            text: qsTr("%")
            Layout.row: 3
            Layout.column: 3
            Layout.columnSpan: 1
            onClicked: calculator.percent()
        }

        GreyButton {
            id: buttonMemAdd
            text: qsTr("M+")
            Layout.row: 2
            Layout.column: 1
            Layout.columnSpan: 1
            onClicked: calculator.memAdd()
        }

        GreyButton {
            id: buttonMemClear
            text: qsTr("M-")
            Layout.row: 2
            Layout.column: 2
            Layout.columnSpan: 1
            onClicked: calculator.memClear()
        }

        GreyButton {
            id: buttonMemRestore
            text: qsTr("M")
            Layout.row: 2
            Layout.column: 3
            Layout.columnSpan: 1
            onClicked: calculator.memRestore()
        }
    }


}
