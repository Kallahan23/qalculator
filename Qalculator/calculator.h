#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QObject>

class Calculator : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString displayValue READ displayValue WRITE setDisplayValue NOTIFY displayChanged)
    Q_PROPERTY(QString displaySign READ displaySign WRITE setDisplaySign NOTIFY displaySignChanged)

public:
    explicit Calculator(QObject *parent = nullptr);
    QString displayValue() const;
    void setDisplayValue(QString newValue);
    QString displaySign() const;
    void setDisplaySign(QString newValue);

signals:
    void displayChanged();
    void displaySignChanged();

public slots:
    void enterDigit(const QString &digit);
    void clearDisplay();
    void setOperation(const QString &sign);
    void equals();
    void changeSign();
    void percent();
    void squareRoot();
    void memAdd();
    void memClear();
    void memRestore();

private:
    QString m_displayValue;
    QString m_displaySign;
    double operate(double value1, char sign, double value2);

};

#endif // CALCULATOR_H
