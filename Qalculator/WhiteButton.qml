import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Button {
    id: button
    Layout.fillHeight: true
    Layout.fillWidth: true

    background: Rectangle {
        color: "#ffffff"
        gradient: Gradient {
            GradientStop { position: 0; color: button.pressed ? "#dadbde" : "#ffffff" }
            GradientStop { position: 1; color: "#ffffff" }
        }
    }

    contentItem: Text {
        color: "black"
        text: parent.text
        font: parent.font
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    onClicked: calculator.enterDigit(text)
}
