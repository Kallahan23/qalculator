import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Button {
    id: button
    Layout.fillHeight: true
    Layout.fillWidth: true

    background: Rectangle {
        color: "#ff9739"
        gradient: Gradient {
            GradientStop { position: 0; color: button.pressed ? "#ff7832" : "#ff9739" }
            GradientStop { position: 1; color: "#ff9739" }
        }
    }

    contentItem: Text {
        color: "white"
        text: parent.text
        font: parent.font
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
}
